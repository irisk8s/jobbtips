from aiohttp import web
from aiohttp_swagger import setup_swagger, swagger_path
from gensim.models import Word2Vec
import requests

routes = web.RouteTableDef()
model = Word2Vec.load("/resources/word2vec_vacancies.model")

@swagger_path("swagger.yaml")
@routes.get('/recommendations/{device_id}')
async def get_recommendations(request):

    device_id = request.match_info['device_id']

    print('get_recommendations: ', device_id)

    return web.json_response([1,234567,345678,456789])

@swagger_path("swagger_recommend.yaml")
@routes.get('/jobrecommendations/{jobad_id}')
async def get_job_recommendations(request):

    jobad_id = request.match_info['jobad_id']
    result = model.most_similar(jobad_id,topn=10)

    total_recommendations = {}
    source_page = requests.post('https://www.arbetsformedlingen.se/rest/pbapi/af/v1/matchning/matchandeRekryteringsbehov/' + jobad_id, json={"profilkriterier":[]})


    total_recommendations["source_jobad_id"] = jobad_id
    total_recommendations["source_jobad_headline"] = source_page.json()["rubrik"]

    jobadlist = []
    for hit in result:
        item = {}
        page = requests.post('https://www.arbetsformedlingen.se/rest/pbapi/af/v1/matchning/matchandeRekryteringsbehov/' + hit[0], json={"profilkriterier":[]})

        item["jobad_id"] = page.json()["id"]
        item["headline"] = page.json()["rubrik"]
        item["location"] = page.json()["erbjudenArbetsplats"]["kommun"]["namn"]
        item["taxonomy_occupation"] = page.json()["yrkesroll"]["namn"]
        jobadlist.append(item)

    total_recommendations["recommended jobads"] = jobadlist

    return web.json_response([total_recommendations])


if __name__ == "__main__":
    app = web.Application()
    app.add_routes(routes)
    setup_swagger(app)
    web.run_app(app)

